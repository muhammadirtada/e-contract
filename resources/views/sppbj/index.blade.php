<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('SPPBJ') }}
        </h2>
        <a href="{{ route('sppbj.create') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
                    className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah SPPBJ
            </button>
        </a>
    </x-slot>

    <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
            <!-- head -->
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nomor SPPBJ</th>
                    <th>Tanggal SPPBJ</th>
                    <th>Penyedia</th>
                    <th>Paket</th>
                    <th>PPK</th>
                    <th>Nilai Penawaran</th>
                    <th>Nilai Negosiasi</th>
                    <th>Unit Kerja</th>
                    <th>Catatan</th>
                    <th>Lampiran Berkas</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sppbjs as $sppbj)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $sppbj->no_sppbj }}</td>
                    <td>{{ $sppbj->tanggal_sppbj }}</td>
                    <td>{{ $sppbj->penyedia->nama_penyedia }}</td>
                    <td>{{ $sppbj->paket->nama_paket }}</td>
                    <td>{{ $sppbj->ppk->nama_ppk }}</td>
                    <td>{{ $sppbj->nilai_penawaran }}</td>
                    <td>{{ $sppbj->nilai_negosiasi }}</td>
                    <td>{{ $sppbj->unitKerja->unit_kerja }}</td>
                    <td>{{ $sppbj->catatan }}</td>
                    <td>{{ $sppbj->lampiran_berkas }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>