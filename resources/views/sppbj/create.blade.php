<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create SPPBJ') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('sppbj.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Nomor SPPBJ</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_sppbj" />

            <label class="label">
                <span class="label-text">Tanggal SPPBJ</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tanggal_sppbj" />

            <label class="label">
                <span class="label-text">Penyedia</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="penyedia">
                <option disabled selected>Pilih Penyedia</option>
                @foreach ($penyedias as $penyedia)
                <option value="{{ $penyedia->id }}">{{ $penyedia->nama_penyedia }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Paket</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="paket">
                <option disabled selected>Pilih Paket</option>
                @foreach ($pakets as $paket)
                <option value="{{ $paket->id }}">{{ $paket->nama_paket }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">PPK</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="ppk">
                <option disabled selected>Pilih PPK</option>
                @foreach ($ppks as $ppk)
                <option value="{{ $ppk->id }}">{{ $ppk->nama_ppk }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Nilai Penawaran</span>
            </label>
            <input type="number" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nilai_penawaran" />

            <label class="label">
                <span class="label-text">Nilai Negosiasi</span>
            </label>
            <input type="number" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nilai_negosiasi" />

            <label class="label">
                <span class="label-text">Unit Kerja</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="unit_kerja">
                <option disabled selected>Pilih Unit Kerja</option>
                @foreach ($unitKerjas as $unitKerja)
                <option value="{{ $unitKerja->id }}">{{ $unitKerja->unit_kerja }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Catatan</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="catatan" />

            <label class="label">
                <span class="label-text">Lampiran Berkas</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="lampiran_berkas" />

            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>