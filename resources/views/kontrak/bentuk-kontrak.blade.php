<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Bentuk Kontrak') }}
        </h2>

        <a href="{{ route('kontrak.index') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                    stroke="currentColor" class="w-5 h-5 m-1">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M9 15L3 9m0 0l6-6M3 9h12a6 6 0 010 12h-3" />
                </svg>
                Back
            </button>
        </a>
    </x-slot>
    <div class="p-5">
        <div class="form-control w-full max-w-xs m-2">
            <form action="{{ route('bentuk-kontrak.index') }}" method="post">
                @csrf
                <label class="label">
                    <span class="label-text">Bentuk Kontrak</span>
                </label>
                <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                    name="bentuk_kontrak" />
                <button class="btn btn-outline btn-primary">Submit</button>
            </form>
        </div>

        <div class="overflow-x-auto">
            <table class="table table-zebra w-full">
                <!-- head -->
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Bentuk Kontrak</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($bentukKontraks as $bentukKontrak)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>{{ $bentukKontrak->bentuk_kontrak }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>