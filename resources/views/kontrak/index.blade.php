<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Kontrak') }}
        </h2>

        <a href="{{ route('kontrak.create') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                    stroke="currentColor" className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round"
                        d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Kontrak
            </button>
        </a>

        <a href="{{ route('bentuk-kontrak.index') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                    stroke="currentColor" className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round"
                        d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Bentuk Kontrak
            </button>
        </a>

        <a href="{{ route('jenis-kontrak.index') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5}
                    stroke="currentColor" className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round"
                        d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Jenis Kontrak
            </button>
        </a>
    </x-slot>

    <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
            <!-- head -->
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nomor Kontrak</th>
                    <th>SPPBJ</th>
                    <th>Tanggal Kontrak</th>
                    <th>Kota Kontrak</th>
                    <th>PPK</th>
                    <th>Penyedia</th>
                    <th>Jenis Kontrak</th>
                    <th>Bentuk Kontrak</th>
                    <th>Waktu Penyelesaian</th>
                    <th>Tanggal Mulai</th>
                    <th>Tanggal Selesai</th>
                    <th>Unit Kerja</th>
                    <th>File</th>
                    <th>Status</th>
                    <th>Keterangan</th>
                    <th>Download Kontrak</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($kontraks as $kontrak)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $kontrak->no_kontrak }}</td>
                    <td>{{ $kontrak->sppbj->no_sppbj }}</td>
                    <td>{{ $kontrak->tanggal_kontrak }}</td>
                    <td>{{ $kontrak->kota_kontrak }}</td>
                    <td>{{ $kontrak->ppk->nama_ppk }}</td>
                    <td>{{ $kontrak->penyedia->nama_penyedia }}</td>
                    <td>{{ $kontrak->jenisKontrak->jenis_kontrak }}</td>
                    <td>{{ $kontrak->bentukKontrak->bentuk_kontrak }}</td>
                    <td>{{ $kontrak->waktu_penyelesaian }}</td>
                    <td>{{ $kontrak->tanggal_mulai }}</td>
                    <td>{{ $kontrak->tanggal_selesai }}</td>
                    <td>{{ $kontrak->sppbj->unitKerja->unit_kerja }}</td>
                    <td>{{ $kontrak->file }}</td>
                    <td>{{ $kontrak->status }}</td>
                    <td>{{ $kontrak->keterangan }}</td>
                    <td>
                        <a href="{{ route('kontrak.download', $kontrak->id) }}">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                                stroke="currentColor" class="w-6 h-6">
                                <path stroke-linecap="round" stroke-linejoin="round"
                                    d="M3 16.5v2.25A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75V16.5M16.5 12L12 16.5m0 0L7.5 12m4.5 4.5V3" />
                            </svg>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>