<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Kontrak') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('kontrak.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Nomor Kontrak</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_kontrak" />

            <label class="label">
                <span class="label-text">SPPBJ</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="sppbj_id">
                <option disabled selected>Pilih SPPBJ</option>
                @foreach ($sppbjs as $sppbj)
                <option value="{{ $sppbj->id }}">{{ $sppbj->no_sppbj }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Tanggal Kontrak</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tanggal_kontrak" />

            <label class="label">
                <span class="label-text">Kota Kontrak</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="kota_kontrak" />

            <label class="label">
                <span class="label-text">PPK</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="ppk_id">
                <option disabled selected>Pilih PPK</option>
                @foreach ($ppks as $ppk)
                <option value="{{ $ppk->id }}">{{ $ppk->nama_ppk }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Penyedia</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="penyedia_id">
                <option disabled selected>Pilih Penyedia</option>
                @foreach ($penyedias as $penyedia)
                <option value="{{ $penyedia->id }}">{{ $penyedia->nama_penyedia }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Jenis Kontrak</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="jenis_kontrak_id">
                <option disabled selected>Pilih Jenis Kontrak</option>
                @foreach ($jenisKontraks as $jenisKontrak)
                <option value="{{ $jenisKontrak->id }}">{{ $jenisKontrak->jenis_kontrak }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Bentuk Kontrak</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="bentuk_kontrak_id">
                <option disabled selected>Pilih Bentuk Kontrak</option>
                @foreach ($bentukKontraks as $bentukKontrak)
                <option value="{{ $bentukKontrak->id }}">{{ $bentukKontrak->bentuk_kontrak }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Waktu Penyelesaian</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="waktu_penyelesaian" />

            <label class="label">
                <span class="label-text">Tanggal Mulai</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tanggal_mulai" />

            <label class="label">
                <span class="label-text">Tanggal Selesai</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tanggal_selesai" />

            <label class="label">
                <span class="label-text">File</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="file" />
                
            <label class="label">
                <span class="label-text">Status</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="status" />

            <label class="label">
                <span class="label-text">Keterangan</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="keterangan" />
            
            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>