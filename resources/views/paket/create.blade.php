<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Paket') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('paket.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Nama Paket</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="paket" />

            <label class="label">
                <span class="label-text">Satker</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="satker">
                <option disabled selected>Pilih Satker</option>
                @foreach ($satkers as $satker)
                <option value="{{ $satker->id }}">{{ $satker->satker }}</option>
                @endforeach
            </select>

            <label class="label">
                <span class="label-text">Nilai Pagu</span>
            </label>
            <input type="number" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nilai_pagu" />

            <label class="label">
                <span class="label-text">Nilai Hps</span>
            </label>
            <input type="number" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nilai_hps" />

            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>