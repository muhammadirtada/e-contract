<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Paket') }}
        </h2>
        <a href="{{ route('paket.create') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
                    className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Paket
            </button>
        </a>
    </x-slot>

    <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
            <!-- head -->
            <thead>
                <tr>
                    <th>#</th>
                    <th>Paket</th>
                    <th>Satker</th>
                    <th>Nilai Pagu</th>
                    <th>Nilai Hps</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pakets as $paket)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $paket->nama_paket }}</td>
                    <td>{{ $paket->satker->satker }}</td>
                    <td>{{ $paket->nilai_pagu }}</td>
                    <td>{{ $paket->nilai_hps }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>