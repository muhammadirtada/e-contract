<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Penyedia') }}
        </h2>
        <a href="{{ route('penyedia.create') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
                    className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Penyedia
            </button>
        </a>
    </x-slot>

    <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
            <!-- head -->
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Penyedia</th>
                    <th>Nama Wakil Penyedia</th>
                    <th>Jabatan Wakil Penyedia</th>
                    <th>Bank</th>
                    <th>Nomor Rekening</th>
                    <th>NPWP Penyedia</th>
                    <th>Nomor KTP Direktur</th>
                    <th>Nomor NPWP Direktur</th>
                    <th>Akta Pendirian</th>
                    <th>Akta Perubahan</th>
                    <th>NIB</th>
                    <th>Alamat</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($penyedias as $penyedia)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $penyedia->nama_penyedia }}</td>
                    <td>{{ $penyedia->nama_wakil_penyedia }}</td>
                    <td>{{ $penyedia->jabatan_wakil_penyedia }}</td>
                    <td>{{ $penyedia->bank }}</td>
                    <td>{{ $penyedia->no_rek }}</td>
                    <td>{{ $penyedia->npwp_penyedia }}</td>
                    <td>{{ $penyedia->no_ktp_direktur }}</td>
                    <td>{{ $penyedia->no_npwp_direktur }}</td>
                    <td>{{ $penyedia->akta_pendirian }}</td>
                    <td>{{ $penyedia->akta_perubahan }}</td>
                    <td>{{ $penyedia->nib }}</td>
                    <td>{{ $penyedia->alamat }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>