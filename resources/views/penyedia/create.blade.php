<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Penyedia') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('penyedia.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Nama Penyedia</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2" name="nama_penyedia" />

            <label class="label">
                <span class="label-text">Nama Wakil Penyedia</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nama_wakil_penyedia" />

            <label class="label">
                <span class="label-text">Jabatan Wakil Penyedia</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="jabatan_wakil_penyedia" />

            <label class="label">
                <span class="label-text">Bank</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="bank" />

            <label class="label">
                <span class="label-text">Nomor Rekening</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_rek" />

            <label class="label">
                <span class="label-text">NPWP Penyedia</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="npwp_penyedia" />

            <label class="label">
                <span class="label-text">Nomor KTP Direktur</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_ktp_direktur" />

            <label class="label">
                <span class="label-text">Nomor NPWP Direktur</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_npwp_direktur" />

            <label class="label">
                <span class="label-text">Akta Pendirian</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="akta_pendirian" />

            <label class="label">
                <span class="label-text">Akta Perubahan</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="akta_perubahan" />

            <label class="label">
                <span class="label-text">NIB</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nib" />

            <label class="label">
                <span class="label-text">Alamat</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="alamat" />

            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>