<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Undangan') }}
        </h2>
        <a href="{{ route('undangan.create') }}">
            <button class="btn btn-outline btn-primary">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
                    className="w-6 h-6" class="w-6 h-6 mr-1">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Tambah Undangan
            </button>
        </a>
    </x-slot>

    <div class="overflow-x-auto">
        <table class="table table-zebra w-full">
            <!-- head -->
            <thead>
                <tr>
                    <th>#</th>
                    <th>Id</th>
                    <th>Paket</th>
                    <th>Tanggal Undangan</th>
                    <th>Jam Mulai</th>
                    <th>Jam Selesai</th>
                    <th>Tempat</th>
                    <th>Harus Bawa</th>
                    <th>Harus Hadir</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($undangans as $undangan)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $undangan->id }}</td>
                    <td>{{ $undangan->paket->nama_paket }}</td>
                    <td>{{ $undangan->tanggal_undangan }}</td>
                    <td>{{ $undangan->tgl_jam_dari }}</td>
                    <td>{{ $undangan->tgl_jam_sampai }}</td>
                    <td>{{ $undangan->tempat }}</td>
                    <td>{{ $undangan->harus_bawa }}</td>
                    <td>{{ $undangan->harus_hadir }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>