<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Undangan') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('undangan.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Paket</span>
            </label>
            <select class="select select-bordered w-full max-w-xs" name="paket">
                <option disabled selected>Pilih Paket</option>
                @foreach ($pakets as $paket)
                <option value="{{ $paket->id }}">{{ $paket->nama_paket }}</option>
                @endforeach
            </select>
            
            <label class="label">
                <span class="label-text">Tanggal Undangan</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2" name="tanggal_undangan" />

            <label class="label">
                <span class="label-text">Jam Mulai</span>
            </label>
            <input type="datetime-local" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tgl_jam_dari" />

            <label class="label">
                <span class="label-text">Jam Selesai</span>
            </label>
            <input type="datetime-local" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tgl_jam_sampai" />

            <label class="label">
                <span class="label-text">Tempat</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tempat" />

            <label class="label">
                <span class="label-text">Harus Bawa</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="harus_bawa" />

            <label class="label">
                <span class="label-text">Harus Hadir</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="harus_hadir" />

            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>