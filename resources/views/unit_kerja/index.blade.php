<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Unit Kerja') }}
        </h2>
    </x-slot>
    <div class="p-5">
        <div class="form-control w-full max-w-xs m-2">
            <form action="{{ route('unit-kerja.store') }}" method="post">
                @csrf
                <label class="label">
                    <span class="label-text">Unit Kerja</span>
                </label>
                <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                    name="unit_kerja" />
                <button class="btn btn-outline btn-primary">Submit</button>
            </form>
        </div>

        <div class="overflow-x-auto">
            <table class="table table-zebra w-full">
                <!-- head -->
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Unit Kerja</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($unitKerjas as $unitKerja)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>{{ $unitKerja->unit_kerja }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>