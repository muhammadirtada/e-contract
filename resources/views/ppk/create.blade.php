<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create PPK') }}
        </h2>
    </x-slot>

    <div class="form-control w-full max-w-xs m-2">
        <form action="{{ route('ppk.store') }}" method="post">
            @csrf
            <label class="label">
                <span class="label-text">Nama PPK</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2" name="nama_ppk" />

            <label class="label">
                <span class="label-text">NIP</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="nip" />

            <label class="label">
                <span class="label-text">Unit</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="unit" />

            <label class="label">
                <span class="label-text">Tanggal Berakhir SK</span>
            </label>
            <input type="date" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="tanggal_berakhir_sk" />

            <label class="label">
                <span class="label-text">PejabatSK</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="pejabat_sk" />

            <label class="label">
                <span class="label-text">Nomor SK</span>
            </label>
            <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                name="no_sk" />

            <button class="btn btn-outline btn-primary">Submit</button>
        </form>
    </div>
</x-app-layout>