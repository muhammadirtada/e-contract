<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Satker') }}
        </h2>
    </x-slot>
    <div class="p-5">
        <div class="form-control w-full max-w-xs m-2">
            <form action="{{ route('satker.store') }}" method="post">
                @csrf
                <label class="label">
                    <span class="label-text">Satker</span>
                </label>
                <input type="text" placeholder="Type here" class="input input-bordered w-full max-w-xs mb-2"
                    name="satker" />
                <button class="btn btn-outline btn-primary">Submit</button>
            </form>
        </div>

        <div class="overflow-x-auto">
            <table class="table table-zebra w-full">
                <!-- head -->
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Satker</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($satkers as $satker)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>{{ $satker->satker }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</x-app-layout>