<?php

use App\Http\Controllers\BentukKontrakController;
use App\Http\Controllers\JenisKontrakController;
use App\Http\Controllers\KontrakController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\PenyediaController;
use App\Http\Controllers\PpkController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SatkerController;
use App\Http\Controllers\SppbjController;
use App\Http\Controllers\UndanganController;
use App\Http\Controllers\UnitKerjaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::resource('satker', SatkerController::class);
    Route::resource('paket', PaketController::class);
    Route::resource('undangan', UndanganController::class);
    Route::resource('ppk', PpkController::class);
    Route::resource('penyedia', PenyediaController::class);
    Route::resource('unit-kerja', UnitKerjaController::class);
    Route::resource('sppbj', SppbjController::class);
    Route::resource('jenis-kontrak', JenisKontrakController::class);
    Route::resource('bentuk-kontrak', BentukKontrakController::class);
    Route::resource('kontrak', KontrakController::class);
    Route::get('kontrak/{kontrak}/download', [KontrakController::class, 'download'])->name('kontrak.download');
});

require __DIR__.'/auth.php';
