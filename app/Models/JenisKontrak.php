<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisKontrak extends Model
{
    use HasFactory;

    public function kontrak()
    {
        return $this->hasMany(Kontrak::class);
    }
}
