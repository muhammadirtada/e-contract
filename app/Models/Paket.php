<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    use HasFactory;

    public function satker()
    {
        return $this->belongsTo(Satker::class);
    }

    public function undangan()
    {
        return $this->hasMany(Undangan::class);
    }

    public function sppbj()
    {
        return $this->hasMany(Sppbj::class);
    }
}
