<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    use HasFactory;

    public function bentukKontrak()
    {
        return $this->belongsTo(BentukKontrak::class);
    }
    public function jenisKontrak()
    {
        return $this->belongsTo(JenisKontrak::class);
    }
    public function ppk()
    {
        return $this->belongsTo(Ppk::class);
    }
    public function penyedia()
    {
        return $this->belongsTo(Penyedia::class);
    }
    public function sppbj()
    {
        return $this->belongsTo(Sppbj::class);
    }
}
