<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sppbj extends Model
{
    use HasFactory;

    public function unitKerja()
    {
        return $this->belongsTo(UnitKerja::class);
    }
    public function paket()
    {
        return $this->belongsTo(Paket::class);
    }
    public function ppk()
    {
        return $this->belongsTo(Ppk::class);
    }
    public function penyedia()
    {
        return $this->belongsTo(Penyedia::class);
    }
    public function kontrak()
    {
        return $this->hasMany(Kontrak::class);
    }
}
