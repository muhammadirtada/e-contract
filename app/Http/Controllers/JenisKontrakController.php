<?php

namespace App\Http\Controllers;

use App\Models\JenisKontrak;
use Illuminate\Http\Request;

class JenisKontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $jenisKontraks = JenisKontrak::orderBy('jenis_kontrak')->get();
        return view('kontrak.jenis-kontrak', compact('jenisKontraks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'jenis_kontrak' => 'required',
        ]);
        $jenisKontrak = new JenisKontrak;
        $jenisKontrak->jenis_kontrak = $validated['jenis_kontrak'];
        $jenisKontrak->save();
        return redirect(route('jenis-kontrak.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(JenisKontrak $jenisKontrak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(JenisKontrak $jenisKontrak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, JenisKontrak $jenisKontrak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(JenisKontrak $jenisKontrak)
    {
        //
    }
}
