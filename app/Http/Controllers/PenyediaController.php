<?php

namespace App\Http\Controllers;

use App\Models\Penyedia;
use Illuminate\Http\Request;

class PenyediaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $penyedias = Penyedia::orderBy('nama_penyedia')->get();
        return view('penyedia.index', compact('penyedias'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('penyedia.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_penyedia' => 'required',
            'nama_wakil_penyedia' => 'required',
            'jabatan_wakil_penyedia' => 'required',
            'bank' => 'required',
            'no_rek' => 'required',
            'npwp_penyedia' => 'required',
            'no_ktp_direktur' => 'required',
            'no_npwp_direktur' => 'required',
            'akta_pendirian' => 'required',
            'akta_perubahan' => 'required',
            'nib' => 'required',
            'alamat' => 'required',
        ]);

        $penyedia = new Penyedia;
        $penyedia->nama_penyedia = $validated['nama_penyedia'];
        $penyedia->nama_wakil_penyedia = $validated['nama_wakil_penyedia'];
        $penyedia->jabatan_wakil_penyedia = $validated['jabatan_wakil_penyedia'];
        $penyedia->bank = $validated['bank'];
        $penyedia->no_rek = $validated['no_rek'];
        $penyedia->npwp_penyedia = $validated['npwp_penyedia'];
        $penyedia->no_ktp_direktur = $validated['no_ktp_direktur'];
        $penyedia->no_npwp_direktur = $validated['no_npwp_direktur'];
        $penyedia->akta_pendirian = $validated['akta_pendirian'];
        $penyedia->akta_perubahan = $validated['akta_perubahan'];
        $penyedia->nib = $validated['nib'];
        $penyedia->alamat = $validated['alamat'];
        $penyedia->save();

        return redirect(route('penyedia.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Penyedia $penyedia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Penyedia $penyedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Penyedia $penyedia)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Penyedia $penyedia)
    {
        //
    }
}
