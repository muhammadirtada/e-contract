<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Undangan;
use Illuminate\Http\Request;

class UndanganController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $undangans = Undangan::all();
        return view('undangan.index', compact('undangans'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $pakets = Paket::orderBy('nama_paket')->get();
        return view('undangan.create', compact('pakets'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'paket' => 'required',
            'tanggal_undangan' => 'required',
            'tgl_jam_dari' => 'required',
            'tgl_jam_sampai' => 'required',
            'tempat' => 'required',
            'harus_bawa' => 'required',
            'harus_hadir' => 'required',
        ]);

        $undangan = new Undangan;
        $undangan->paket_id = $validated['paket'];
        $undangan->tanggal_undangan = $validated['tanggal_undangan'];
        $undangan->tgl_jam_dari = $validated['tgl_jam_dari'];
        $undangan->tgl_jam_sampai = $validated['tgl_jam_sampai'];
        $undangan->tempat = $validated['tempat'];
        $undangan->harus_bawa = $validated['harus_bawa'];
        $undangan->harus_hadir = $validated['harus_hadir'];
        $undangan->save();

        return redirect(route('undangan.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Undangan $undangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Undangan $undangan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Undangan $undangan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Undangan $undangan)
    {
        //
    }
}
