<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Penyedia;
use App\Models\Ppk;
use App\Models\Sppbj;
use App\Models\UnitKerja;
use Illuminate\Http\Request;

class SppbjController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sppbjs = Sppbj::orderBy('no_sppbj')->get();
        return view('sppbj.index', compact('sppbjs'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $penyedias = Penyedia::orderBy('nama_penyedia')->get();
        $pakets = Paket::orderBy('nama_paket')->get();
        $ppks = Ppk::orderBy('nama_ppk')->get();
        $unitKerjas = UnitKerja::orderBy('unit_kerja')->get();

        return view('sppbj.create', [
            'penyedias' => $penyedias,
            'pakets' => $pakets,
            'ppks' => $ppks,
            'unitKerjas' => $unitKerjas,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'no_sppbj' => 'required',
            'tanggal_sppbj' => 'required',
            'penyedia' => 'required',
            'paket' => 'required',
            'ppk' => 'required',
            'nilai_penawaran' => 'required',
            'nilai_negosiasi' => 'required',
            'unit_kerja' => 'required',
            'catatan' => 'required',
            'lampiran_berkas' => 'required',
        ]);

        $sppbj = new Sppbj;
        $sppbj->no_sppbj = $validated['no_sppbj'];
        $sppbj->tanggal_sppbj = $validated['tanggal_sppbj'];
        $sppbj->penyedia_id = $validated['penyedia'];
        $sppbj->paket_id = $validated['paket'];
        $sppbj->ppk_id = $validated['ppk'];
        $sppbj->nilai_penawaran = $validated['nilai_penawaran'];
        $sppbj->nilai_negosiasi = $validated['nilai_negosiasi'];
        $sppbj->unit_kerja_id = $validated['unit_kerja'];
        $sppbj->catatan = $validated['catatan'];
        $sppbj->lampiran_berkas = $validated['lampiran_berkas'];
        $sppbj->save();

        return redirect(route('sppbj.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Sppbj $sppbj)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Sppbj $sppbj)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Sppbj $sppbj)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Sppbj $sppbj)
    {
        //
    }
}
