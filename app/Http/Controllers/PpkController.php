<?php

namespace App\Http\Controllers;

use App\Models\Ppk;
use Illuminate\Http\Request;

class PpkController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $ppks = Ppk::orderBy('nama_ppk')->get();
        return view('ppk.index', compact('ppks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('ppk.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_ppk' => 'required',
            'nip' => 'required',
            'unit' => 'required',
            'tanggal_berakhir_sk' => 'required',
            'pejabat_sk' => 'required',
            'no_sk' => 'required',
        ]);

        $ppk = new Ppk;
        $ppk->nama_ppk = $validated['nama_ppk'];
        $ppk->nip = $validated['nip'];
        $ppk->unit = $validated['unit'];
        $ppk->tanggal_berakhir_sk = $validated['tanggal_berakhir_sk'];
        $ppk->pejabat_sk = $validated['pejabat_sk'];
        $ppk->no_sk = $validated['no_sk'];
        $ppk->save();

        return redirect(route('ppk.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Ppk $ppk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Ppk $ppk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Ppk $ppk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Ppk $ppk)
    {
        //
    }
}
