<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Satker;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pakets = Paket::orderBy('nama_paket')->get();
        return view('paket.index', compact('pakets'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $satkers = Satker::orderBy('satker')->get();
        return view('paket.create', compact('satkers'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'paket' => 'required',
            'satker' => 'required',
            'nilai_pagu' => 'required',
            'nilai_hps' => 'required',
        ]);

        $paket = new Paket;
        $paket->nama_paket = $validated['paket'];
        $paket->satker_id = $validated['satker'];
        $paket->nilai_pagu = $validated['nilai_pagu'];
        $paket->nilai_hps = $validated['nilai_hps'];
        $paket->save();

        return redirect(route('paket.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Paket $paket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Paket $paket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Paket $paket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Paket $paket)
    {
        //
    }
}
