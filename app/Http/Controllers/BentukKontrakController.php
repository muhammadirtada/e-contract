<?php

namespace App\Http\Controllers;

use App\Models\BentukKontrak;
use Illuminate\Http\Request;

class BentukKontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $bentukKontraks = BentukKontrak::orderBy('bentuk_kontrak')->get();
        return view('kontrak.bentuk-kontrak', compact('bentukKontraks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'bentuk_kontrak' => 'required',
        ]);
        $bentukKontrak = new BentukKontrak;
        $bentukKontrak->bentuk_kontrak = $validated['bentuk_kontrak'];
        $bentukKontrak->save();
        return redirect(route('bentuk-kontrak.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(BentukKontrak $bentukKontrak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BentukKontrak $bentukKontrak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, BentukKontrak $bentukKontrak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BentukKontrak $bentukKontrak)
    {
        //
    }
}
