<?php

namespace App\Http\Controllers;

use App\Models\BentukKontrak;
use App\Models\JenisKontrak;
use App\Models\Kontrak;
use App\Models\Penyedia;
use App\Models\Ppk;
use App\Models\Sppbj;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;

class KontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kontraks = Kontrak::orderBy('no_kontrak')->get();
        return view('kontrak.index', compact('kontraks'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $sppbjs = Sppbj::orderBy('no_sppbj')->get();
        $ppks = Ppk::orderBy('nama_ppk')->get();
        $penyedias = Penyedia::orderBy('nama_penyedia')->get();
        $jenisKontraks = JenisKontrak::orderBy('jenis_kontrak')->get();
        $bentukKontraks = BentukKontrak::orderBy('bentuk_kontrak')->get();

        return view('kontrak.create', [
            'sppbjs' => $sppbjs,
            'ppks' => $ppks,
            'penyedias' => $penyedias,
            'jenisKontraks' => $jenisKontraks,
            'bentukKontraks' => $bentukKontraks,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'no_kontrak' => 'required',
            'sppbj_id' => 'required',
            'tanggal_kontrak' => 'required',
            'kota_kontrak' => 'required',
            'ppk_id' => 'required',
            'penyedia_id' => 'required',
            'jenis_kontrak_id' => 'required',
            'bentuk_kontrak_id' => 'required',
            'waktu_penyelesaian' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'file' => 'required',
            'status' => 'required',
            'keterangan' => 'required',
        ]);

        $kontrak = new Kontrak;
        $kontrak->no_kontrak = $validated['no_kontrak'];
        $kontrak->sppbj_id = $validated['sppbj_id'];
        $kontrak->tanggal_kontrak = $validated['tanggal_kontrak'];
        $kontrak->kota_kontrak = $validated['kota_kontrak'];
        $kontrak->ppk_id = $validated['ppk_id'];
        $kontrak->penyedia_id = $validated['penyedia_id'];
        $kontrak->jenis_kontrak_id = $validated['jenis_kontrak_id'];
        $kontrak->bentuk_kontrak_id = $validated['bentuk_kontrak_id'];
        $kontrak->waktu_penyelesaian = $validated['waktu_penyelesaian'];
        $kontrak->tanggal_mulai = $validated['tanggal_mulai'];
        $kontrak->tanggal_selesai = $validated['tanggal_selesai'];
        $kontrak->file = $validated['file'];
        $kontrak->status = $validated['status'];
        $kontrak->keterangan = $validated['keterangan'];
        $kontrak->save();

        return redirect(route('kontrak.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Kontrak $kontrak)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kontrak $kontrak)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Kontrak $kontrak)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kontrak $kontrak)
    {
        //
    }

    public function download(Kontrak $kontrak)
    {
        // $phpWord = new \PhpOffice\PhpWord\TemplateProcessor('kontrak-satu.docx');
        // $phpWord1 = new \PhpOffice\PhpWord\PhpWord();
        $templateProcessor = new TemplateProcessor('kontrak-satu.docx');

        $templateProcessor->setValues([
            'tanggal_mulai' => $kontrak->tanggal_mulai,
            'waktu_penyelesaian' => $kontrak->waktu_penyelesaian,
            'kota_kontrak' => $kontrak->kota_kontrak,
            'tanggal_kontrak' => $kontrak->tanggal_kontrak,
        ]);


        // Saving the document as HTML file...
        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord1, 'HTML');
        // $objWriter->save('template_kontrak.html');
        $templateProcessor->saveAs("Kontrak_$kontrak->no_kontrak.docx");

        return response()->download("Kontrak_$kontrak->no_kontrak.docx")->deleteFileAfterSend(true);
    }
}
