<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('undangans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('paket_id')->constrained('pakets')->onUpdate('cascade');
            $table->date('tanggal_undangan');
            $table->dateTime('tgl_jam_dari');
            $table->dateTime('tgl_jam_sampai');
            $table->string('tempat');
            $table->text('harus_bawa');
            $table->text('harus_hadir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('undangans');
    }
};
