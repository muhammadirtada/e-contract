<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kontraks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('sppbj_id')->constrained('sppbjs')->onDelete('no action')->onUpdate('no action');
            $table->string('no_kontrak', 100);
            $table->date('tanggal_kontrak');
            $table->string('kota_kontrak', 50);
            $table->foreignId('ppk_id')->constrained('ppks')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('penyedia_id')->constrained('penyedias')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('jenis_kontrak_id')->constrained('jenis_kontraks')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('bentuk_kontrak_id')->constrained('bentuk_kontraks')->onDelete('no action')->onUpdate('no action');
            $table->string('waktu_penyelesaian', 100);
            $table->date('tanggal_mulai');
            $table->date('tanggal_selesai');
            $table->text('file');
            $table->integer('status');
            $table->string('keterangan', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kontraks');
    }
};
