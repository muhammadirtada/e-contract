<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ppks', function (Blueprint $table) {
            $table->id();
            $table->string('nama_ppk', 200);
            $table->string('nip', 100);
            $table->string('unit', 100);
            $table->date('tanggal_berakhir_sk');
            $table->string('pejabat_sk', 200);
            $table->string('no_sk', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ppks');
    }
};
