<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sppbjs', function (Blueprint $table) {
            $table->id();
            $table->string('no_sppbj', 50);
            $table->date('tanggal_sppbj');
            $table->foreignId('penyedia_id')->constrained('penyedias')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('paket_id')->constrained('pakets')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('ppk_id')->constrained('ppks')->onDelete('no action')->onUpdate('no action');
            $table->bigInteger('nilai_penawaran');
            $table->bigInteger('nilai_negosiasi');
            $table->foreignId('unit_kerja_id')->constrained('unit_kerjas')->onDelete('no action')->onUpdate('no action');
            $table->text('catatan');
            $table->text('lampiran_berkas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sppbjs');
    }
};
