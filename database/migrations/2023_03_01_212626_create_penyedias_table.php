<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penyedias', function (Blueprint $table) {
            $table->id();
            $table->text('nama_penyedia');
            $table->string('nama_wakil_penyedia', 100);
            $table->string('jabatan_wakil_penyedia', 100);
            $table->string('bank', 100);
            $table->string('no_rek', 100);
            $table->string('npwp_penyedia', 100);
            $table->string('no_ktp_direktur', 100);
            $table->string('no_npwp_direktur', 100);
            $table->string('akta_pendirian', 100);
            $table->string('akta_perubahan', 100);
            $table->string('nib', 100);
            $table->text('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penyedias');
    }
};
